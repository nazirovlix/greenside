<?php
/**
 * Created by PhpStorm.
 * User: MrLIX
 */
//////////////////////// FRONTEND //////////////////////////////
///
/// TITLES
///
public function getTitle(){
    if(Yii::$app->language == 'ru') { return $this->title_ru; }
    if(Yii::$app->language == 'uz') { return $this->title_uz; }
    if(Yii::$app->language == 'en') { return $this->title_en; }
}
/// CONTENT
///
public function getContent(){
    if(Yii::$app->language == 'ru') { return $this->content_ru; }
    if(Yii::$app->language == 'uz') { return $this->content_uz; }
    if(Yii::$app->language == 'en') { return $this->content_en; }
}

//-------




///////////////////////// ADMIN ///////////////////////////////
// ======== Image Path =========
// Behaviors
public function behaviors()
{
    return [
        [
            'class' => '\app\components\FileUploadBehaviour'
        ],

    ];
}

// Form view ?>

<?= $form->field($model, 'path')->fileInput(['class' => 'dropify', 'data-default-file' => '/uploads/' . $model->path]) ?>

<?php

// For View or Index
[
    'attribute' => 'path',
    'value' => function($model){
        return '<img src="/uploads/'.$model->path.'" width="150px">';
    },
    'format' => 'raw',

],
//------------------------

// Status
    [
        'attribute' => 'status',
        'value' => function($model){
            if($model->status == 1):
                return '<span style="color: #0b58a2;">Published</span>';
            else:
                return '<span style="color: #ff0c3e">Not published</span>';

            endif;

        },
        'format' => 'html',
        'filter' => [
            '1' => 'Pulished',
            '0' => 'Not published'
        ],
    ],

//--------------------------------------


// Order
 <?= $form->field($model, 'order')->dropDownList([
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
    '6' => '6',
    '7' => '7',
    '8' => '8',
    '9' => '9',
    '10' => '10',
]) ?>
