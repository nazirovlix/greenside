CREATE TABLE `header` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `path` varchar(255) NULL,
  `title1_ru` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `title1_en` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `title2_ru` varchar(255) COLLATE 'utf8_general_ci' NULL,
  `title2_en` varchar(255) COLLATE 'utf8_general_ci' NULL,
  `time1_ru` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `time1_en` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `time2_ru` varchar(255) COLLATE 'utf8_general_ci' NULL,
  `time2_en` varchar(255) COLLATE 'utf8_general_ci' NULL,
  `phone` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `address_ru` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `address_en` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `lat` varchar(255) COLLATE 'utf8_general_ci' NULL,
  `lng` varchar(255) COLLATE 'utf8_general_ci' NULL
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `about` (
 `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `title_ru` VARCHAR(255) not null,
 `title_en` VARCHAR(255) not null,
 `content_ru` text null,
 `content_en` text null,
 `created_at` int(11) null,
 `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `about_images` (
 `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `about_id` int(11) not null,
 `image` VARCHAR(255) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `category` (
 `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `title_ru` VARCHAR(255) not null,
 `title_en` VARCHAR(255) not null,
 `status` SMALLINT(6) null,
 `created_at` int(11) null,
 `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `sub_category` (
 `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `category_id` int(11) not null,
 `title_ru` VARCHAR(255) not null,
 `title_en` VARCHAR(255) not null,
 `status` SMALLINT(6) null,
 `created_at` int(11) null,
 `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `products` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `path` varchar(255) NULL,
  `title_ru` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `title_en` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `content_ru` text null,
  `content_en` text null,
  `price` int(11) null,
  `status` SMALLINT(6) null,
  `created_at` int(11) null,
  `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `discounts` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `path` varchar(255) NULL,
  `title_ru` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `title_en` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `content_ru` text null,
  `content_en` text null,
  `created_at` int(11) null,
  `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `policy` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `path` varchar(255) NULL,
  `title_ru` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `title_en` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `order` SMALLINT (6) null,
  `status` SMALLINT(6) null,
  `created_at` int(11) null,
  `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `projects` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `path` varchar(255) NULL,
  `title_ru` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `title_en` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `content_ru` text null,
  `content_en` text null,
  `order` SMALLINT (6) null,
  `status` SMALLINT(6) null,
  `created_at` int(11) null,
  `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

create table `socials` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `path` varchar(255) NULL,
  `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `url` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `order` SMALLINT (6) null,
  `status` SMALLINT(6) null,
  `created_at` int(11) null,
  `updated_at` int(11) null
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

























