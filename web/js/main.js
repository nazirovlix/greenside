$('#application').submit(function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var method = $(this).attr('method');
    var data = $(this).serialize();

    $.ajax({
        url: url,  // Url::to(['/site/application'])
        type: method, // GET
        data: data,    // name, phone, country
        success: function (res) {

        },
        error: function (e) {
            alert('Error');
        }


    });

});