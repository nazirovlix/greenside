<?php

use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'title_ru',
            'title_en',
           // 'parent_id',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 1):
                        return '<span style="color: #0b58a2;">Published</span>';
                    else:
                        return '<span style="color: #ff0c3e">Not published</span>';

                    endif;

                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Pulished',
                    '0' => 'Not published'
                ],
            ],
            [
                'attribute' => 'parent_id',
                'value' => function($model){
                    if(!empty($model->parent)){
                        return $model->parent->title_ru;
                    } else{
                        return '-';
                    }
                },
                'filter' => ArrayHelper::map(Category::find()->where(['status' => 1, 'parent_id' => Null])->all(),'id','title_ru'),
            ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
