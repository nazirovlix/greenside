<?php
/**
 * Created by PhpStorm.
 * User: Farhodjon
 * Date: 10.03.2018
 * Time: 15:17
 */

use app\modules\admin\widgets\Menu;
use yii\helpers\Url;

?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php
            try {
                echo Menu::widget([
                    'options' => [ 'id' => 'sidebarnav' ],
                    'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                    'badgeClass' => 'label label-rouded label-primary pull-right',
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => '',
                            'options' => [ 'class' => 'nav-devider' ]
                        ],
                        [
                            'label' => 'Home',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Dashboard',
                            'url' => ['default/index'],
                            'icon' => '<i class="fa fa-tachometer"></i>',
                        ],
                        [
                            'label' => 'App',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Header',
                            'url' => Url::to(['header/view','id' => 1]),
                            'icon' => '<i class="fa fa-home"></i>',
                        ],
                        [
                            'label' => 'About',
                            'url' => Url::to(['about/view','id' => 1]),
                            'icon' => '<i class="fa fa-info-circle"></i>',
                        ],
                        [
                            'label' => 'Tasting card',
                            'url' => '#',
                            'icon' => '<i class="fa fa-th"></i>',
                            'items' => [
                                [
                                    'label' => 'Category',
                                    'url' => Url::to(['category/index']),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Products',
                                    'url' => ['products/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],


                            ]
                        ],
                        [
                            'label' => 'Discounts',
                            'url' => Url::to(['discounts/index']),
                            'icon' => '<i class="fa fa-credit-card"></i>',
                        ],
                        [
                            'label' => 'Policy',
                            'url' => Url::to(['policy/index']),
                            'icon' => '<i class="fa fa-shield"></i>',
                        ],
                        [
                            'label' => 'Future projects',
                            'url' => Url::to(['projects/index']),
                            'icon' => '<i class="fa fa-navicon"></i>',
                        ],
                        [
                            'label' => 'Socials',
                            'url' => Url::to(['socials/index']),
                            'icon' => '<i class="fa fa-facebook-official"></i>',
                        ],
                        [
                            'label' => 'Application',
                            'url' => Url::to(['application/index']),
                            'icon' => '<i class="fa fa-envelope"></i>',
                        ],

                    ]
                ]);
            } catch ( Exception $e ) {
            }
            
            ?>
        </nav>
    </div>
</div>
