<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\About */

$this->title = 'About';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Abouts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title_ru',
            'title_en',
            'content_ru:ntext',
            'content_en:ntext',
            'created_at:date',
            'updated_at:date',
            [
                'attribute' => 'Images',
                'value' => function($model){
                    $return = '';
                    foreach ($model->aboutImages as $item) {
                        $return .= '<img src="/uploads/' . $item->image . '" width="100px"> ';
                    }
                    return $return;
                },
                'format' => 'raw',

            ],
        ],
    ]) ?>

</div>
