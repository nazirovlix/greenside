<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Header */

$this->title = 'Header';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Headers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="header-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'path',
                'value' => function($model){
                    return '<img src="/uploads/'.$model->path.'" width="200px">';
                },
                'format' => 'raw',

            ],
            'title1_ru',
            'title1_en',
            'title2_ru',
            'title2_en',
            'time1_ru',
            'time1_en',
            'time2_ru',
            'time2_en',
            'phone',
            'address_ru',
            'address_en',
            'lat',
            'lng',
        ],
    ]) ?>

    <input id="lat" class="hidden" value="<?= $model->lat; ?>" />
    <input id="lng" class="hidden" value="<?= $model->lng; ?>" />

    <div class="field" id="map2" style="height: 400px; width: 100%; margin-top: 30px">
        <script>



            function initMap( ) {
                var lat1 = Number(document.getElementById("lat").value);
                var lng1 = Number(document.getElementById("lng").value);
                var uluru = {lat: lat1, lng: lng1};
                var map = new google.maps.Map(document.getElementById('map2'), {
                    zoom: 15,
                    center: uluru

                });
                var marker = new google.maps.Marker({

                    position: uluru,
                    map: map
                });
            }


        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
        </script>

    </div>

</div>
