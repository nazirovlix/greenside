<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Header */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="header-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'path')->fileInput(['class' => 'dropify', 'data-default-file' => '/uploads/' . $model->path]) ?>

    <div class="row">
        <div class="col-md-4">
            <label> Titles </label>
            <?= $form->field($model, 'title1_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title1_en')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title2_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title2_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <label> Time </label>
            <?= $form->field($model, 'time1_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'time1_en')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'time2_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'time2_en')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-4">
            <label> Phone and Address </label>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone_to_call')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'lat')->textInput(['maxlength' => true,'class' => 'hidden'])->label(false) ?>
            <?= $form->field($model, 'lng')->textInput(['maxlength' => true,'class' => 'hidden'])->label(false) ?>
        </div>
    </div>



    <div class="ordering-map" style="margin-bottom: 30px">

        <div class="ordering-map-h4" style="margin-bottom: 10px">
            <h4>
                <?= Yii::t('app', 'Address') ?>
            </h4>
        </div>
        <div class="ordering-map-img">

            <div id="floating-panel">
                <input onclick="deleteMarkers();" type=button value="Delete marker">
            </div>
            <div class="field" id="map" style="height: 300px; width: 100%;">
                <script>
                    var map;
                    var markers = [];
                    var i = 1;
                    var lat;
                    var lng;

                    function initMap() {
                        // var lat1 = Number(document.getElementById("contacts-lat").value);
                        // var lng1 = Number(document.getElementById("contacts-lng").value);
                        var haightAshbury = {lat: 55.751244, lng: 37.618423};
                        ;


                        map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 13,
                            center: haightAshbury,
                            mapTypeId: 'terrain'
                        });


                        // This event listener will call addMarker() when the map is clicked.
                        map.addListener('click', function (event) {
                            deleteMarkers();
                            if (i == 1) {
                                addMarker(event.latLng);
                                i = 2;
                            }
                        });
                    }


                    // Adds a marker to the map and push to the array.
                    function addMarker(location) {

                        var marker = new google.maps.Marker({
                            position: location,
                            map: map
                        });
                        markers.push(marker);

                        $('#header-lat').val(marker.getPosition().lat());
                        $('#header-lng').val(marker.getPosition().lng());

                    }


                    // Sets the map on all markers in the array.
                    function setMapOnAll(map) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(map);
                        }

                    }

                    // Removes the markers from the map, but keeps them in the array.
                    function clearMarkers() {
                        setMapOnAll(null);
                    }

                    // Shows any markers currently in the array.
                    function showMarkers() {
                        setMapOnAll(map);
                    }

                    // Deletes all markers in the array by removing references to them.
                    function deleteMarkers() {
                        clearMarkers();
                        i = 1;
                        $('#header-lat').val('');
                        $('#header-lng').val('');
                        markers = [];
                    }


                </script>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                </script>

            </div>

        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
