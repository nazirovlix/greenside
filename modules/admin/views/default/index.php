<?php
use yii\helpers\Url;

?>
<div class="row">
    <div class="col-lg-12">
        <div class="card p-30">
            <div class="media home-images">
                <div class="img-header">
                    <a href="<?= Url::to(['header/view', 'id' => 1]) ?>">
                        <img src="/images/green/header.jpg" onmouseover="hoverHeader(this);" onmouseout="unhoverHeader(this);"/>
                    </a>
                </div>
                <div class="img-about">
                    <a href="<?= Url::to(['about/view','id' => 1]) ?>">
                        <img src="/images/green/about.jpg" onmouseover="hoverAbout(this);" onmouseout="unhoverAbout(this);" />
                    </a>
                </div>
                <div class="img-categories">
                    <a href="<?= Url::to(['category/index']) ?>">
                        <img src="/images/green/categories.jpg" onmouseover="hoverCategories(this);" onmouseout="unhoverCategories(this);" />
                    </a>
                </div>
                <div class="img-products">
                    <a href="<?= Url::to(['products/index']) ?>">
                        <img src="/images/green/products.jpg" onmouseover="hoverProducts(this);" onmouseout="unhoverProducts(this);" />
                    </a>
                </div>
                <div class="img-discounts">
                    <a href="<?= Url::to(['discounts/index']) ?>">
                        <img src="/images/green/discounts.jpg" onmouseover="hoverDiscounts(this);" onmouseout="unhoverDiscounts(this);" />
                    </a>
                </div>
                <div class="img-policy">
                    <a href="<?= Url::to(['policy/index']) ?>">
                        <img src="/images/green/policy.jpg" onmouseover="hoverPolicy(this);" onmouseout="unhoverPolicy(this);" />
                    </a>
                </div>
                <div class="img-projects">
                    <a href="<?= Url::to(['projects/index']) ?>">
                        <img src="/images/green/projects.jpg" onmouseover="hoverProjects(this);" onmouseout="unhoverProjects(this);" />
                    </a>
                </div>
                <div class="img-socials">
                    <a href="<?= Url::to(['socials/index']) ?>">
                        <img src="/images/green/socials.jpg" onmouseover="hoverSocials(this);" onmouseout="unhoverSocials(this);" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start Page Content -->
<div class="row">
    <div class="col-md-6">
        <div class="card p-30">
            <div class="media">

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card p-30">
            <div class="media">

            </div>
        </div>
    </div>
</div>

<script>
//    Header
    function hoverHeader(element) {
        element.setAttribute('src', '/images/green/header_hover.JPG');
    }
    function unhoverHeader(element) {
        element.setAttribute('src', '/images/green/header.JPG');
    }
//    About
    function hoverAbout(element) {
        element.setAttribute('src', '/images/green/about_hover.JPG');
    }
    function unhoverAbout(element) {
        element.setAttribute('src', '/images/green/about.JPG');
    }
//    Categories
    function hoverCategories(element) {
        element.setAttribute('src', '/images/green/categories_hover.JPG');
    }
    function unhoverCategories(element) {
        element.setAttribute('src', '/images/green/categories.JPG');
    }
//    Products
    function hoverProducts(element) {
        element.setAttribute('src', '/images/green/products_hover.JPG');
    }
    function unhoverProducts(element) {
        element.setAttribute('src', '/images/green/products.JPG');
    }

//    Discounts
    function hoverDiscounts(element) {
        element.setAttribute('src', '/images/green/discounts_hover.JPG');
    }
    function unhoverDiscounts(element) {
        element.setAttribute('src', '/images/green/discounts.JPG');
    }
//    Policy
    function hoverPolicy(element) {
        element.setAttribute('src', '/images/green/policy_hover.JPG');
    }
    function unhoverPolicy(element) {
        element.setAttribute('src', '/images/green/policy.JPG');
    }
//    Projects
    function hoverProjects(element) {
        element.setAttribute('src', '/images/green/projects_hover.JPG');
    }
    function unhoverProjects(element) {
        element.setAttribute('src', '/images/green/projects.JPG');
    }
//    Socials
    function hoverSocials(element) {
        element.setAttribute('src', '/images/green/socilas_hover.JPG');
    }
    function unhoverSocials(element) {
        element.setAttribute('src', '/images/green/socials.JPG');
    }
</script>