<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'path',
                'value' => function($model){
                    return '<img src="/uploads/'.$model->path.'" width="100px">';
                },
                'format' => 'raw',

            ],
            'title_ru',
            //'title_en',
            //'content_ru:ntext',
            //'content_en:ntext',
            'price',
            'power',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 1):
                        return '<span style="color: #0b58a2;">Published</span>';
                    else:
                        return '<span style="color: #ff0c3e">Not published</span>';

                    endif;

                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Pulished',
                    '0' => 'Not published'
                ],
            ],
            //'created_at',
            'updated_at:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
