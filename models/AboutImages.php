<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about_images".
 *
 * @property int $id
 * @property int $about_id
 * @property string $image
 *
 * @property About $about
 */
class AboutImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['about_id'], 'required'],
            [['about_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['about_id'], 'exist', 'skipOnError' => true, 'targetClass' => About::className(), 'targetAttribute' => ['about_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'about_id' => Yii::t('app', 'About ID'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbout()
    {
        return $this->hasOne(About::className(), ['id' => 'about_id']);
    }
}
