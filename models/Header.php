<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "header".
 *
 * @property int $id
 * @property string $path
 * @property string $title1_ru
 * @property string $title1_en
 * @property string $title2_ru
 * @property string $title2_en
 * @property string $time1_ru
 * @property string $time1_en
 * @property string $time2_ru
 * @property string $time2_en
 * @property string $phone
 * @property string $address_ru
 * @property string $address_en
 * @property string $lat
 * @property string $lng
 */
class Header extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'header';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title1_ru', 'title1_en', 'time1_ru', 'time1_en', 'phone', 'address_ru', 'address_en'], 'required'],
            [['title1_ru', 'title1_en', 'title2_ru', 'title2_en', 'time1_ru', 'time1_en', 'time2_ru', 'time2_en', 'phone', 'phone_to_call', 'address_ru', 'address_en', 'lat', 'lng'], 'string', 'max' => 255],
            [['path'], 'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Path'),
            'title1_ru' => Yii::t('app', 'Title1 Ru'),
            'title1_en' => Yii::t('app', 'Title1 En'),
            'title2_ru' => Yii::t('app', 'Title2 Ru'),
            'title2_en' => Yii::t('app', 'Title2 En'),
            'time1_ru' => Yii::t('app', 'Time1 Ru'),
            'time1_en' => Yii::t('app', 'Time1 En'),
            'time2_ru' => Yii::t('app', 'Time2 Ru'),
            'time2_en' => Yii::t('app', 'Time2 En'),
            'phone' => Yii::t('app', 'Phone'),
            'phone_to_call' => Yii::t('app', 'Phone to Call'),
            'address_ru' => Yii::t('app', 'Address Ru'),
            'address_en' => Yii::t('app', 'Address En'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
        ];
    }
}
