<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_en
 * @property int $created_at
 * @property int $updated_at
 *
 * @property AboutImages[] $aboutImages
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    public $attachments;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],

            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'aboutImages',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en'], 'required'],
            [['content_ru', 'content_en'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title_ru', 'title_en'], 'string', 'max' => 255],
            [['attachments'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_en' => Yii::t('app', 'Title En'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_en' => Yii::t('app', 'Content En'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'attachments' => Yii::t('app', 'Images'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutImages()
    {
        return $this->hasMany(AboutImages::className(), ['about_id' => 'id']);
    }
}
