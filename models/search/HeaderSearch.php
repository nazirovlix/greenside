<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Header;

/**
 * HeaderSearch represents the model behind the search form of `app\models\Header`.
 */
class HeaderSearch extends Header
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['path', 'title1_ru', 'title1_en', 'title2_ru', 'title2_en', 'time1_ru', 'time1_en', 'time2_ru', 'time2_en', 'phone', 'address_ru', 'address_en', 'lat', 'lng'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Header::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'title1_ru', $this->title1_ru])
            ->andFilterWhere(['like', 'title1_en', $this->title1_en])
            ->andFilterWhere(['like', 'title2_ru', $this->title2_ru])
            ->andFilterWhere(['like', 'title2_en', $this->title2_en])
            ->andFilterWhere(['like', 'time1_ru', $this->time1_ru])
            ->andFilterWhere(['like', 'time1_en', $this->time1_en])
            ->andFilterWhere(['like', 'time2_ru', $this->time2_ru])
            ->andFilterWhere(['like', 'time2_en', $this->time2_en])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address_ru', $this->address_ru])
            ->andFilterWhere(['like', 'address_en', $this->address_en])
            ->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'lng', $this->lng]);

        return $dataProvider;
    }
}
