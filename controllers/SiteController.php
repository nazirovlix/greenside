<?php

namespace app\controllers;

use app\models\About;
use app\models\Application;
use app\models\Category;
use app\models\Discounts;
use app\models\forms\Login;
use app\models\Header;
use app\models\Policy;
use app\models\Projects;
use app\models\Socials;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {


        $header = Header::findOne(['id' => 1]);
        $about = About::findOne(['id' => 1]);
        $categories = Category::find()
            ->where(['status' => 1])
            ->all();
        $discounts = Discounts::find()
            ->limit(3)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $policy = Policy::find()
            ->where(['status' => 1])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $projects = Projects::find()
            ->where(['status' => 1])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $socials = Socials::find()
            ->where(['status' => 1])
            ->orderBy(['order' => SORT_ASC])
            ->all();

        return $this->render('index', compact([
            'header',
            'about',
            'categories',
            'discounts',
            'policy',
            'projects',
            'socials'
        ]));
    }


    public function actionApplication(){

        $name = Yii::$app->request->get('name');
        $phone = Yii::$app->request->get('phone');
        $country = Yii::$app->request->get('country');
        
        $application  = new Application();
        $application->name = $name;
        $application->phone = $phone;
        $application->country = $country;

        if($application->save()){
            Yii::$app->session->setFlash('app',true);
            $application = new Application();
            return true;
        } else{
            return false;
        }

    }
    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
